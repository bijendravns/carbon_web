<?php
class ModelCatalogStaticblock extends Model {
	
	public function addBlock($data) {
		$link = (isset($data['link']))?$data['link']:'';
		$this->db->query("INSERT INTO " . DB_PREFIX . "staticblock SET on_page = '" . $this->db->escape($data['on_page']) . "', image = '" . $this->db->escape($data['image']) . "', block_position = '" . $this->db->escape($data['block_position']) . "', link = '" . $this->db->escape($link) . "', date_added = NOW()");

		$block_id = $this->db->getLastId();

		foreach ($data['product_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "staticblock_desc SET block_id = '" . (int)$block_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
		return $block_id;
	}

	public function editBlock($block_id, $data) {
		$link = (isset($data['link']))?$data['link']:'';
		$this->db->query("UPDATE " . DB_PREFIX . "staticblock SET on_page = '" . $this->db->escape($data['on_page']) . "', image = '" . $this->db->escape($data['image']) . "', block_position = '" . $this->db->escape($data['block_position']) . "', link = '" . $this->db->escape($link) . "' where block_id ='".(int)$block_id."'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "staticblock_desc WHERE block_id = '" . (int)$block_id . "'");

		foreach ($data['product_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "staticblock_desc SET block_id = '" . (int)$block_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

	}

	public function deleteBlock($block_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "static_block WHERE block_id = '" . (int)$block_id . "'");
	}
	
	public function getBlocks($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "staticblock s LEFT JOIN " . DB_PREFIX . "staticblock_desc sd ON (s.block_id = sd.block_id) WHERE sd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$sql .= " GROUP BY s.block_id";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getTotalBlocks($data = array()) {
		$sql = "SELECT COUNT(DISTINCT s.block_id) AS total FROM " . DB_PREFIX . "staticblock s LEFT JOIN " . DB_PREFIX . "staticblock_desc sd ON (s.block_id = sd.block_id)";

		$sql .= " WHERE sd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getStaticblockDescriptions($block_id) {
		$block_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "staticblock_desc WHERE block_id = '" . (int)$block_id . "'");

		foreach ($query->rows as $result) {
			$block_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description']
			);
		}

		return $block_description_data;
	}
	
	public function getBlock($block_id) {
		
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "staticblock sb LEFT JOIN " . DB_PREFIX . "staticblock_desc sbd ON (sb.block_id = sbd.block_id) WHERE sb.block_id = '" . (int)$block_id . "' AND sbd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	} 
}
