<?php
class ModelCatalogTestimonial extends Model {
	
	public function addTestimonial($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "testimonial SET image = '" . $this->db->escape($data['image']) . "', date_added = NOW()");

		$testimonial_id = $this->db->getLastId();

		foreach ($data['product_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "testimonial_desc SET testimonial_id = '" . (int)$testimonial_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
		return $testimonial_id;
	}

	public function editTestimonial($testimonial_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "testimonial SET image = '" . $this->db->escape($data['image']) . "' where testimonial_id ='".(int)$testimonial_id."'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "testimonial_desc WHERE testimonial_id = '" . (int)$testimonial_id . "'");

		foreach ($data['product_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "testimonial_desc SET testimonial_id = '" . (int)$testimonial_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

	}

	public function deleteTestimonials($testimonial_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "testimonial WHERE testimonial_id = '" . (int)$testimonial_id . "'");
	}
	
	public function getTestimonials($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "testimonial s LEFT JOIN " . DB_PREFIX . "testimonial_desc sd ON (s.testimonial_id = sd.testimonial_id) WHERE sd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$sql .= " GROUP BY s.testimonial_id";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getTotalTestimonials($data = array()) {
		$sql = "SELECT COUNT(DISTINCT s.testimonial_id) AS total FROM " . DB_PREFIX . "testimonial s LEFT JOIN " . DB_PREFIX . "testimonial_desc sd ON (s.testimonial_id = sd.testimonial_id)";

		$sql .= " WHERE sd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getTestimonialDescriptions($testimonial_id) {
		$block_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "testimonial_desc WHERE testimonial_id = '" . (int)$testimonial_id . "'");

		foreach ($query->rows as $result) {
			$block_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description']
			);
		}

		return $block_description_data;
	}
	
	public function getTestimonial($testimonial_id) {
		
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "testimonial sb LEFT JOIN " . DB_PREFIX . "testimonial_desc sbd ON (sb.testimonial_id = sbd.testimonial_id) WHERE sb.testimonial_id = '" . (int)$testimonial_id . "' AND sbd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	} 
}
