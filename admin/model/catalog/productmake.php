<?php
class ModelCatalogProductmake extends Model {
	
	public function addProductmake($data) {
		foreach ($data['order_status'] as $language_id => $value) {
			if (isset($id)) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "make SET id = '" . (int)$id . "', language_id = '" . (int)$language_id . "', make = '" . $this->db->escape($value['name']) . "'");
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "make SET language_id = '" . (int)$language_id . "', make = '" . $this->db->escape($value['name']) . "'");

				$id = $this->db->getLastId();
			}
		}

		$this->cache->delete('make');
		
		return $id;
	}

	public function editProductmake($id, $data) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "make WHERE id = '" . (int)$id . "'");

		foreach ($data['order_status'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "make SET id = '" . (int)$id . "', language_id = '" . (int)$language_id . "', make = '" . $this->db->escape($value['name']) . "'");
		}

		$this->cache->delete('make');

	}

	public function deleteProductmakes($id) {
		$row = $this->getProductmake($id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "model WHERE make_id = '" . $row['make'] . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "make WHERE id = '" . (int)$id . "'");
	}
	
	public function getProductmakes($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "make WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sql .= " ORDER BY make";

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$make_data = $this->cache->get('make.' . (int)$this->config->get('config_language_id'));

			if (!$make_data) {
				$query = $this->db->query("SELECT id, make FROM " . DB_PREFIX . "make WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY make");

				$make_data = $query->rows;

				$this->cache->set('make.' . (int)$this->config->get('config_language_id'), $make_data);
			}

			return $make_data;
		}
	}
	
	public function getTotalProductmakes($data = array()) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "make WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row['total'];
	}
	
	public function getProductmakesDescriptions($id) {
		$make_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "make WHERE id = '" . (int)$id . "'");

		foreach ($query->rows as $result) {
			$make_data[$result['language_id']] = array('name' => $result['make']);
		}

		return $make_data;
	}
	
	public function getProductmake($id) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "make WHERE id = '" . (int)$id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	} 
}
