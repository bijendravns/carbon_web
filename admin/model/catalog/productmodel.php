<?php
class ModelCatalogProductmodel extends Model {
	
	public function addProductmodel($data) {
		foreach ($data['order_status'] as $language_id => $value) {
			if (isset($id)) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "model SET id = '" . (int)$id . "', language_id = '" . (int)$language_id . "', model =  '" . $this->db->escape($value['name']) . "', make_id =  '" . $this->db->escape($value['make_id']) . "'");
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "model SET language_id = '" . (int)$language_id . "', model =  '" . $this->db->escape($value['name']) . "', make_id =  '" . $this->db->escape($value['make_id']) . "'");

				$id = $this->db->getLastId();
			}
		}

		$this->cache->delete('model');
		
		return $id;
	}

	public function editProductmodel($id, $data) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "model WHERE id = '" . (int)$id . "'");

		foreach ($data['order_status'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "model SET id = '" . (int)$id . "', language_id = '" . (int)$language_id . "', model =  '" . $this->db->escape($value['name']) . "', make_id =  '" . $this->db->escape($value['make_id']) . "'");
		}

		$this->cache->delete('model');

	}

	public function deleteProductmodels($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "model WHERE id = '" . (int)$id . "'");
	}
	
	public function getProductmodels($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "model WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sql .= " ORDER BY model";

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$make_data = $this->cache->get('model.' . (int)$this->config->get('config_language_id'));

			if (!$make_data) {
				$query = $this->db->query("SELECT id, model FROM " . DB_PREFIX . "model WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY model");

				$make_data = $query->rows;

				$this->cache->set('model.' . (int)$this->config->get('config_language_id'), $make_data);
			}

			return $make_data;
		}
	}
	
	public function getTotalProductmodels($data = array()) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "model WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row['total'];
	}
	
	public function getProductmodelsDescriptions($id) {
		$make_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "model WHERE id = '" . (int)$id . "'");

		foreach ($query->rows as $result) {
			$make_data[$result['language_id']] = array('name' => $result['model'], 'make_id' => $result['make_id']);
		}

		return $make_data;
	}
	
	public function getProductmodel($id) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "model WHERE id = '" . (int)$id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	} 
	
	public function getProductmakes($data = array()) {
		
		$make_data = array();
		
		$sql = "SELECT * FROM " . DB_PREFIX . "make";

		$sql .= " ORDER BY make";
		
		$query = $this->db->query($sql);
		
		foreach ($query->rows as $result) {
			$make_data[$result['language_id']][] = array('name' => $result['make']);
		}


		return $make_data;
	}
}
