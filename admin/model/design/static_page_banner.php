<?php
class ModelDesignStaticPageBanner extends Model {
	public function addBanner($data) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "static_banner WHERE name = '" . $this->db->escape($data['name']) . "'");

		if($query->row){
			
			return array('is_banner' => true);
			
		} else {
			$this->db->query("INSERT INTO " . DB_PREFIX . "static_banner SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "'");

			$banner_id = $this->db->getLastId();

			foreach ($data['banner_description'] as $language_id => $value) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "static_banner_image SET banner_id = '" . (int)$banner_id . "', language_id = '" . (int)$language_id . "', image = '" . $this->db->escape($value['image']) . "'");
			}
			
			$this->cache->delete('static_banner');

			return $banner_id;			
		}
		
	}

	public function editBanner($banner_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "static_banner SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "' WHERE banner_id = '" . (int)$banner_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "static_banner_image WHERE banner_id = '" . (int)$banner_id . "'");

		foreach ($data['banner_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "static_banner_image SET banner_id = '" . (int)$banner_id . "', language_id = '" . (int)$language_id . "', image = '" . $this->db->escape($value['image']) . "'");
		}

		$this->cache->delete('static_banner');
	}

	public function deleteBanner($banner_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "static_banner WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "static_banner_image WHERE banner_id = '" . (int)$banner_id . "'");

		$this->cache->delete('static_banner');
	}

	public function getBanner($banner_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "static_banner r LEFT JOIN " . DB_PREFIX . "static_banner_image rd ON (r.banner_id = rd.banner_id) WHERE r.banner_id = '" . (int)$banner_id . "' AND rd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getBanners($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "static_banner r LEFT JOIN " . DB_PREFIX . "static_banner_image rd ON (r.banner_id = rd.banner_id) WHERE rd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND r.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (isset($data['filter_image']) && !is_null($data['filter_image'])) {
			if ($data['filter_image'] == 1) {
				$sql .= " AND (rd.image IS NOT NULL AND rd.image <> '' AND rd.image <> 'no_image.png')";
			} else {
				$sql .= " AND (rd.image IS NULL OR rd.image = '' OR rd.image = 'no_image.png')";
			}
		}

		$sql .= " GROUP BY r.banner_id";

		$sort_data = array(
			'r.name',
			'r.status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY r.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getBannerDescriptions($banner_id) {
		$static_banner_image_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "static_banner_image WHERE banner_id = '" . (int)$banner_id . "'");

		foreach ($query->rows as $result) {
			$static_banner_image_data[$result['language_id']] = array(
				'image'       => $result['image']
			);
		}

		return $static_banner_image_data;
	}

	public function getTotalBanners($data = array()) {
		$sql = "SELECT COUNT(DISTINCT r.banner_id) AS total FROM " . DB_PREFIX . "static_banner r LEFT JOIN " . DB_PREFIX . "static_banner_image rd ON (r.banner_id = rd.banner_id)";

		$sql .= " WHERE rd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND r.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND r.status = '" . (int)$data['filter_status'] . "'";
		}

		if (isset($data['filter_image']) && !is_null($data['filter_image'])) {
			if ($data['filter_image'] == 1) {
				$sql .= " AND (rd.image IS NOT NULL AND rd.image <> '' AND rd.image <> 'no_image.png')";
			} else {
				$sql .= " AND (rd.image IS NULL OR rd.image = '' OR rd.image = 'no_image.png')";
			}
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}
