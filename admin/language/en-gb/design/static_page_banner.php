<?php
// Heading
$_['heading_title']          = 'Banners';

// Text
$_['text_error']          	 = 'Error: Banner for this page is already exist!';
$_['text_success']           = 'Success: You have modified banner!';
$_['text_list']              = 'Banner List';
$_['text_add']               = 'Add Banner';
$_['text_edit']              = 'Edit Banner';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';

// Column
$_['column_name']            = 'Banner On Page';
$_['column_image']           = 'Image';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

$_['text_img_note']          = 'Note: Upload Only 1399 X 465 px Image for best resolution';

// Entry
$_['entry_name']             = 'Page Title';
$_['entry_image']            = 'Image';
$_['entry_text']             = 'Text';
$_['entry_option']           = 'Option';
$_['entry_option_value']     = 'Option Value';
$_['entry_required']         = 'Required';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';

// Help
$_['help_name']             = 'Select page title on which banner will display';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Page name is required!';
$_['error_main_image']       = 'Image is required';
