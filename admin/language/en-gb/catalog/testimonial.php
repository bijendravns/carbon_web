<?php
// Heading
$_['heading_title']          = 'Testimonials';

// Text
$_['text_success']           = 'Success: You have modified testimonial!';
$_['text_list']              = 'Testimonials List';
$_['text_add']               = 'Add Testimonial';
$_['text_edit']              = 'Edit Testimonial';
$_['text_filter']            = 'Filter';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';
$_['text_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Column
$_['column_image']           = 'Image';
$_['column_name']           = 'Name';
$_['column_action']          = 'Action';

// Entry
$_['entry_section']      	= 'Name';
$_['entry_description']       = 'description';
$_['entry_image'] = 'Image';

// Help
$_['help_page']           = 'Select page on which this module will be display';
$_['help_section']           = 'Enter position where this module will be display';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Name must be greater than 1 and less than 100 characters!';