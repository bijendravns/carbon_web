<?php
class ControllerAccountPassword extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/password', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/password');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('account/customer');

			$this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password_confirmation']);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('account/password', '', true));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/password', '', true)
		);
		
		if(isset($this->session->data['success'])){
			$data['success'] = $this->session->data['success'];			
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if (isset($this->error['old_password'])) {
			$data['error_old_password'] = $this->error['old_password'];
		} else {
			$data['error_old_password'] = '';
		}

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		if (isset($this->error['confirm'])) {
			$data['error_confirm'] = $this->error['confirm'];
		} else {
			$data['error_confirm'] = '';
		}

		$data['action'] = $this->url->link('account/password', '', true);

		if (isset($this->request->post['password_confirmation'])) {
			$data['password'] = $this->request->post['password_confirmation'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['password'])) {
			$data['confirm'] = $this->request->post['password'];
		} else {
			$data['confirm'] = '';
		}

		$data['back'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header', array('banner' => 'change_password'));

		$this->response->setOutput($this->load->view('account/password', $data));
	}

	protected function validate() {
		
		if ($this->request->post['old_password'] == '') {
			$this->error['old_password'] = $this->language->get('error_old_password');
		} else {
			$this->load->model('account/customer');
			$response = $this->model_account_customer->verifyOldPassword($this->customer->getEmail(), $this->request->post['old_password']);
			if(!$response){
				$this->error['old_password'] = $this->language->get('error_wrong_old_password');
			}
		}
		
		if ((utf8_strlen(html_entity_decode($this->request->post['password_confirmation'], ENT_QUOTES, 'UTF-8')) < 8) || (utf8_strlen(html_entity_decode($this->request->post['password_confirmation'], ENT_QUOTES, 'UTF-8')) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['password'] != $this->request->post['password_confirmation']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		return !$this->error;
	}
}