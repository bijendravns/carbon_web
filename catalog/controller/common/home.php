<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}
		
		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
		
		$data['base_url'] = $server;
		$this->load->model('tool/image');
		
		//Testimonials
		$this->load->model('common/testimonial');
		$data['testimonials'] = array();
		$testimonials = $this->model_common_testimonial->getTestimonials();
		if(!empty($testimonials)){
			$i = 0;
			foreach($testimonials as $testimonial){
				if($i == 0)
					$active = true;
				else
					$active = false;
				if (is_file(DIR_IMAGE . $testimonial['image'])) {
					$image = $this->model_tool_image->resize($testimonial['image'], 100, 100);
				} else {
					$image = $this->model_tool_image->resize('no_image.png', 100, 100);
				}
				$data['testimonials'][] = array(
					'active' => $active,
					'image' => $image,
					'name' => $testimonial['name'],
					'description' => strip_tags(html_entity_decode($testimonial['description'])),
				);
				++$i;
			}
		}
		
		/*
		* All static blocks here ->
		*/
		
		
		$this->load->model('common/staticblock');
		$block_info1 = $this->model_common_staticblock->getStaticblock(array('block_id'=>1,'on_page'=>'home'));
		$block_info2 = $this->model_common_staticblock->getStaticblock(array('block_id'=>2,'on_page'=>'home'));
		$block_info3 = $this->model_common_staticblock->getStaticblock(array('block_id'=>3,'on_page'=>'home'));
		$block_info4 = $this->model_common_staticblock->getStaticblock(array('block_id'=>4,'on_page'=>'home'));
		$block_info5 = $this->model_common_staticblock->getStaticblock(array('block_id'=>5,'on_page'=>'home'));
		$block_info6 = $this->model_common_staticblock->getStaticblock(array('block_id'=>6,'on_page'=>'home'));
		$block_info7 = $this->model_common_staticblock->getStaticblock(array('block_id'=>7,'on_page'=>'home'));
		$block_info9 = $this->model_common_staticblock->getStaticblock(array('block_id'=>9,'on_page'=>'home'));
		
		if(!empty($block_info1)){
			if (is_file(DIR_IMAGE . $block_info1['image'])) {
				$image = $data['base_url'].'image/'.$block_info1['image'];
			} else {
				$image = $data['base_url'].'image/no_image.png';
			}
			$block_info1['image'] = $image;
			$data['more_department_1'] = $block_info1;
		}
		
		if(!empty($block_info2)){
			if (is_file(DIR_IMAGE . $block_info2['image'])) {
				$image = $data['base_url'].'image/'.$block_info2['image'];
			} else {
				$image = $data['base_url'].'image/no_image.png';
			}
			$block_info2['image'] = $image;
			$data['more_department_2'] = $block_info2;
		}
		
		if(!empty($block_info3)){
			if (is_file(DIR_IMAGE . $block_info3['image'])) {
				$image = $data['base_url'].'image/'.$block_info3['image'];
			} else {
				$image = $data['base_url'].'image/no_image.png';
			}
			$block_info3['image'] = $image;
			$data['more_department_3'] = $block_info3;
		}
		
		if(!empty($block_info4)){
			if (is_file(DIR_IMAGE . $block_info4['image'])) {
				$image = $data['base_url'].'image/'.$block_info4['image'];
			} else {
				$image = $data['base_url'].'image/no_image.png';
			}
			$block_info4['image'] = $image;
			$data['more_department_4'] = $block_info4;
		}
		
		if(!empty($block_info5)){
			if (is_file(DIR_IMAGE . $block_info5['image'])) {
				$image = $data['base_url'].'image/'.$block_info5['image'];
			} else {
				$image = $data['base_url'].'image/no_image.png';
			}
			$block_info5['image'] = $image;
			$data['more_department_5'] = $block_info5;
		}
		
		if(!empty($block_info6)){
			if (is_file(DIR_IMAGE . $block_info6['image'])) {
				$image = $data['base_url'].'image/'.$block_info6['image'];
			} else {
				$image = $data['base_url'].'image/no_image.png';
			}
			$block_info6['image'] = $image;
			$data['more_department_6'] = $block_info6;
		}
		
		if(!empty($block_info7)){
			if (is_file(DIR_IMAGE . $block_info7['image'])) {
				$image = $data['base_url'].'image/'.$block_info7['image'];
			} else {
				$image = $data['base_url'].'image/no_image.png';
			}
			$block_info7['image'] = $image;
			$data['more_department_7'] = $block_info7;
		}
		
		if(!empty($block_info9)){
			$data['must_have_every_car'] = $block_info9;
		}
		
		/*
		* All static blocks here end <-
		*/

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		// Menu
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$data['categories'] = array();
		$categories = $this->model_catalog_category->getCategories(0);
		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();
				$children = $this->model_catalog_category->getCategories($category['category_id']);
				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);
					
					if (is_file(DIR_IMAGE . $child['image'])) {
						$image = $data['base_url'].'image/'.$child['image'];
					} else {
						$image = $data['base_url'].'image/no_image.png';
					}
					
					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						//'image' => $this->model_tool_image->resize($child['image'],300, 200),
						'image' => $image,
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}
				// Level 1
				$data['categories'][] = array(
					'category_id'     => $category['category_id'],
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}
		
		$data['top_categories'] = array();
		$ex = 0;
		foreach ($categories as $category) {
			if($ex == 4)
				break;
			if ($category['top']) {
				// Level 1				
				if (is_file(DIR_IMAGE . $category['image'])) {
					$image = $data['base_url'].'image/'.$category['image'];
				} else {
					$image = $data['base_url'].'image/no_image.png';
				}
				
				$filter_data = array(
					'filter_category_id'  => $category['category_id'],
					'filter_sub_category' => false
				);
				$data['top_categories'][] = array(
					'product_count' => ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'category_id'     => $category['category_id'],
					'name'     => $category['name'],
					'image' => $image,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
			++$ex;
		}

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
