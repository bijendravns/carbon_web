<?php
class ControllerCommonHomeSearch extends Controller {
	public function index() {
	}
	
	public function get_model() {
		$this->load->model('common/home_search');
		$data['all_models'] = array();
		$all_models = $this->model_common_home_search->getModels($this->request->get['make']);
		$html = '<option value="">Select Model</option>';
		if(!empty($all_models)){
			foreach($all_models as $model){
				$html .= '<option value="'. $model['model'] .'">'. $model['model'] .'</option>';
			}
		}
        $this->response->setOutput(json_encode($html));
    }
}