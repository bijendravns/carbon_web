<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}
		
		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
		
		$data['base'] = $server;
		
		$data['lang'] = $this->language->get('code');
		$data['site_email'] = $this->config->get('config_email');
		$data['site_phone'] = $this->config->get('config_telephone');
		$data['site_address'] = $this->config->get('config_address');

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['tracking'] = $this->url->link('information/tracking');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/login', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = ($this->request->server['HTTPS'] ? 'https://' : 'http://') . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}
		
		/*
		* All static blocks here ->
		*/
		$this->load->model('common/staticblock');
		$block_info8 = $this->model_common_staticblock->getStaticblock(array('block_id'=>8,'on_page'=>'footer'));
		$block_info10 = $this->model_common_staticblock->getStaticblock(array('block_id'=>10,'on_page'=>'footer'));
		$block_info11 = $this->model_common_staticblock->getStaticblock(array('block_id'=>11,'on_page'=>'footer'));
		$block_info12 = $this->model_common_staticblock->getStaticblock(array('block_id'=>12,'on_page'=>'footer'));
		
		if(!empty($block_info8)){
			$data['footer_after_newsletter'] = $block_info8;
		}	
		
		if(!empty($block_info10)){
			$data['footer_after_logo_text'] = $block_info10;
		}	
		
		if(!empty($block_info11)){
			$data['footer_copyright_text'] = $block_info11;
		}
		
		if(!empty($block_info12)){
			$data['footer_payment_logo'] = $block_info12;
		}
		/*
		* All static blocks here end <-
		*/
		
		$data['name'] = $this->config->get('config_name');
		
		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}
		
		if($this->config->get('config_social_link')){
			$data['social_links'] = array();			
			$social_links = $this->config->get('config_social_link');
			foreach($social_links as $key => $value){
				if(strpos($value, '|')){
					list($link, $class) = explode('|', $value);
					$data['social_links'][$link] = $class;					
				}
			}
		} else {
			$data['social_links'] = array();
		}
		
		unset($this->session->data['redirect']);
		if(isset($this->request->get['route']) && $this->request->get['route'] == 'account/logout'){
			//$login_href = $this->url->link('account/logout', '', true);
			$this->session->data['redirect'] = 'account/logout';
		}
		

		$data['scripts'] = $this->document->getScripts('footer');
		
		return $this->load->view('common/footer', $data);
	}
}
