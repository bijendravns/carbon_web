<?php
class ControllerNewsletterNewsletter extends Controller {
	
	public function subscribe() {
		$this->load->model('newsletter/newsletter');
		$this->load->language('newsletter/newsletter');
        $response = array();
        if ($this->validateEmail()) {
			$isSubscribed = $this->model_newsletter_newsletter->isSubscribed($this->request->post['email']);
            if ($isSubscribed) {
                $response['status'] = 'error';
                $response['unsubscribe'] = 1;
                $response['message'] = $this->language->get('msg_subscribed');
            } else {
                $this->model_newsletter_newsletter->subscribe($this->request->post['email']);
                $response['status'] = 'success';
                $response['message'] = $this->language->get('msg_subscribe_success');
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = $this->language->get('msg_invalid_email');
        }
        $this->response->setOutput(json_encode($response));
    }

    public function unsubscribe() {
		$this->load->model('newsletter/newsletter');
		$this->load->language('newsletter/newsletter');
        $response = array();
        if ($this->validateEmail()) {
			$isSubscribed = $this->model_newsletter_newsletter->isSubscribed($this->request->post['email']);
            if ($isSubscribed) {
                $this->model_newsletter_newsletter->unsubscribe($this->request->post['email']);
				
			// Mailchimp integration
					// MailChimp API credentials
					$apiKey = '12979f2254da960f2158e0309b18932e-us18';
					$listID = 'b5b5edda6c';
					
					// MailChimp API URL
					$memberID = md5(strtolower($this->request->post['email']));
					$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
					$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
					// member information
					$json = json_encode([
						'email_address' => $this->request->post['email'],
						'status'        => 'unsubscribed'/* ,
						'merge_fields'  => [
							'FNAME'     => $fname,
							'LNAME'     => $lname
						] */
					]);
					
					// send a HTTP POST request with curl
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
					curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_TIMEOUT, 10);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
					$result = curl_exec($ch);
					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					curl_close($ch);
					//hp_pr($result);
					// store the status message based on response code
					if ($httpCode == 200) {
						$response['message'] = '<p style="color: #34A853">You have successfully subscribed.</p>';
					} else {
						switch ($httpCode) {
							case 214:
								$msg = 'You are already subscribed.';
								break;
							default:
								$msg = 'Some problem occurred, please try again.';
								break;
						}
						$response['message'] = '<p style="color: #EA4335">'.$msg.'</p>';
					}		

			//				
				
                $response['status'] = 'success';
                $response['message'] = $this->language->get('msg_unsubscribed_success');
            } else {
                $response['status'] = 'error';
                $response['message'] = $this->language->get('msg_no_email');
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = $this->language->get('msg_invalid_email');
        }
        $this->response->setOutput(json_encode($response));
    }

    private function validateEmail() {
        return isset($this->request->post['email']) && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email']);
    }
}
