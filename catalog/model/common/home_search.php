<?php
class ModelCommonHomeSearch extends Model {

	public function getMakes($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "make WHERE status = 1";
		$query = $this->db->query($sql);
		return $query->rows;
	}	
	
	public function getModels($make) {
		$sql = "SELECT * FROM " . DB_PREFIX . "model WHERE status = 1 AND make_id='". $make ."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
}
