<?php
class ModelCommonStaticblock extends Model {

 	public function getStaticblock($data = array()) {

		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "staticblock sb LEFT JOIN " . DB_PREFIX . "staticblock_desc sbd ON (sb.block_id = sbd.block_id) WHERE sbd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

 		if (!empty($data['block_id'])) {
			$sql .= " AND sb.block_id = " . (int)$this->db->escape($data['block_id']);
		}
		
 		if (!empty($data['on_page'])) {
			$sql .= " AND sb.on_page = '" . $this->db->escape($data['on_page']) . "'";
		}	

		if (isset($data['status'])) {
			$sql .= " AND sb.status = '" . $this->db->escape($data['status']) . "'";
		}
		
		$query = $this->db->query($sql);
		return $query->row;
	} 

	public function getStaticblocks($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "static_block sb LEFT JOIN " . DB_PREFIX . "static_block_description sbd ON (sb.block_id = sbd.block_id) WHERE sbd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

 		if (!empty($data['filter_name'])) {
			$sql .= " AND sbd.block_title LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status'])) {
			$sql .= " AND sb.status = '" . $this->db->escape($data['filter_status']) . "'";
		}
		if (!empty($data['filter_block'])) {
			$sql .= " AND sb.block_page LIKE '" . $this->db->escape($data['filter_block']) . "'";
		}

		$sql .= " GROUP BY sb.block_id";

		$sort_data = array(
			'sbd.block_title',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sbd.block_title";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getStaticblockDescriptions($block_id) {
		$block_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "static_block_description WHERE block_id = '" . (int)$block_id . "'");

		foreach ($query->rows as $result) {
			$block_description_data[$result['language_id']] = array(
				'name'             => $result['block_title'],
				'description'      => $result['description']
			);
		}

		return $block_description_data;
	}

	public function getTotalStaticblocks($data = array()) {
		$sql = "SELECT COUNT(DISTINCT sb.block_id) AS total FROM " . DB_PREFIX . "static_block sb LEFT JOIN " . DB_PREFIX . "static_block_description sbd ON (sb.block_id = sbd.block_id)";
		$sql .= " WHERE sbd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}
