<?php
class ModelCommonTestimonial extends Model {

 	public function getTestimonials($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "testimonial s LEFT JOIN " . DB_PREFIX . "testimonial_desc sd ON (s.testimonial_id = sd.testimonial_id) WHERE sd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$sql .= " GROUP BY s.testimonial_id";
		
		$sql .= " ORDER BY s.testimonial_id DESC";

		$query = $this->db->query($sql);
		
		return $query->rows;
	}
}
