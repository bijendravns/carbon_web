<?php
// Text
$_['text_subject']      = '%s - Order %s';
$_['text_received']     = 'received.';
$_['text_order_id']     = 'Order ID:';
$_['text_date_added']   = 'Date Added:';
$_['text_order_status'] = 'Order Status:';
$_['text_product']      = 'Products';
$_['text_total']        = 'Totals';
$_['text_order_details']= 'Order Details';
$_['text_comment']      = 'The comments for your order are:';

//new 
$_['text_dear']= 'Dear';
$_['text_order_details']= 'Order Details';
$_['text_item']      = 'Item';
$_['text_qty']      = 'Qty';
$_['text_uprice']      = 'Unit Price';
$_['text_tax']      = 'Tax';
$_['text_amount']      = 'Amount';
$_['text_support']      = 'For further queries or support, you can contact our Customer Support';
$_['text_rights']      = 'All rights reserved';
$_['text_thanks']= 'Thank you for shopping at';
$_['text_order']= 'Order';
$_['text_placed']= 'is placed';
$_['text_order_shipped']= 'Your order has been placed. You will be notified when we ship';
