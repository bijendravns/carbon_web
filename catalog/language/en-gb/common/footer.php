<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_signup']   = 'Signup';
$_['text_customer_service']   = 'Customer Service';
$_['text_account_signup']   = 'Account Signup';
$_['text_account_register']   = 'Register';
$_['text_ind_user']   = 'Individual User';
$_['text_dealer']   = 'Dealer';
$_['text_let_start']   = 'LET\'S START';
$_['text_login']   = 'PLEASE LOGIN';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';

$_['forgot_email_text']   = 'Enter the e-mail address associated with your account. Click submit to have a password reset link e-mailed to you.';

$_['place_email']   = 'Enter mail id';

$_['entry_customer_group'] = 'Customer Group';
$_['entry_bussinessname']      = 'Business Name';
$_['entry_contactname']      = 'Contact Name';
$_['entry_firstname']      = 'First Name';
$_['entry_lastname']       = 'Last Name';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Contact Number';
$_['entry_taxid']      = 'Taxid Number';
$_['entry_newsletter']     = 'Subscribe';
$_['entry_password']       = 'Password';
$_['entry_confirm']        = 'Password Confirm';

$_['btn_account']        = 'Create Your Account';
$_['btn_login']        = 'LOG IN';
$_['btn_submit']        = 'SUBMIT';
$_['btn_click_here']        = 'Click Here';
$_['have_account']        = 'Already Have Account?';
$_['have_dont_account']        = 'Don\'t Have Account?';
$_['forgot_password']        = 'Forget Your Password?';
$_['have_login']        = 'Login?';

$_['tnc_1']        = 'By creating account you are aggreeing to our';
$_['tnc_2']        = 'Privacy Policy and Terms Of Use';

