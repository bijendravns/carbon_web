<?php
// Text
$_['text_newsletter']   = 'SIGN UP FOR NEWSLETTER';

// Button
$_['btn_newsletter']   	= 'SUBSCRIBE';

// Placeholder
$_['placeholder_newsletter']   	= 'Your email address';

// Message
$_['msg_subscribed']   			= 'You have already subscribed';
$_['msg_subscribe_success']   	= 'Thank you for subscribing on our newsletter.';
$_['msg_unsubscribed_success']  = 'You have been unsubscribed from our newsletter.';
$_['msg_no_email']   			= 'Your E-Mail was not found.';
$_['msg_invalid_email']   		= 'Your E-Mail is invalid.';
