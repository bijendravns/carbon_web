<?php
// Heading
$_['heading_title']  = 'Change Password';

// Text
$_['text_account']   = 'Account';
$_['text_password']  = 'Your Password';
$_['text_success']   = 'Success: Your password has been successfully updated.';

// Entry
$_['entry_old_password'] = 'Old Password';
$_['entry_password'] = 'Password';
$_['entry_confirm']  = 'Password Confirm';

// Error
$_['error_old_password'] = 'Old password is required!';
$_['error_wrong_old_password'] = 'Old password is wrong!';
$_['error_password'] = 'Password must be between 8 and 20 characters!';
$_['error_confirm']  = 'Password confirmation does not match password!';