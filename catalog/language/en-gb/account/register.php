<?php
// Heading
$_['heading_title']        = 'Register Account';

// Text
$_['text_account']         = 'Account';
$_['text_register']        = 'Register';
$_['text_account_already'] = 'If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_your_details']    = 'Your Personal Details';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Customer Group';
$_['entry_firstname']      = 'First Name';
$_['entry_lastname']       = 'Last Name';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Telephone';
$_['entry_newsletter']     = 'Subscribe';
$_['entry_password']       = 'Password';
$_['entry_confirm']        = 'Password Confirm';

// Error
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_phone_exists']   = 'Warning: Phone number is already registered!';
$_['error_contactname']      = 'Contact Name must be between 2 and 32 characters!';
$_['error_firstname']      = 'First Name must be between 2 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 2 and 32 characters!';
$_['error_business_name']  = 'Business Name must be between 2 and 255 characters!';
$_['error_taxid']  		   = 'Tax Id must be between 1 and 100 characters!';
$_['error_taxid_exist']   = 'Warning: Tax Id already exist!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 10 and 20 characters!';
$_['error_telephonen']      = 'Telephone must be number';
$_['error_custom_field']   = '%s required!';
$_['error_password']       = 'Password must be between 8 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_agree']          = 'Warning: You must agree to the %s!';