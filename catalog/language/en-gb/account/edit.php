<?php
// Heading
$_['heading_title']      = 'My Account Information';

// Text
$_['text_account']       = 'Account';
$_['text_edit']          = 'Edit Information';
$_['text_your_details']  = 'Your Personal Details';
$_['text_success']       = 'Success: Your account has been successfully updated.';

// Entry
$_['entry_firstname']    = 'First Name';
$_['entry_lastname']     = 'Last Name';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Telephone';

$_['entry_bussinessname']      = 'Business Name';
$_['entry_contactname']      = 'Contact Name';
$_['entry_taxid']      = 'Taxid Number';

// Error
$_['error_exists']       = 'Warning: E-Mail address is already registered!';
$_['error_phone_exists'] = 'Warning: Phone number is already registered!';
$_['error_firstname']    = 'First Name must be between 2 and 32 characters!';
$_['error_lastname']     = 'Last Name must be between 2 and 32 characters!';
$_['error_email']        = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']    = 'Telephone must be between 10 and 20 characters!';

$_['error_contactname']      = 'Contact Name must be between 2 and 32 characters!';
$_['error_business_name']  = 'Business Name must be between 2 and 255 characters!';
$_['error_taxid']  		   = 'Tax Id must be between 1 and 100 characters!';
$_['error_taxid_exist']   = 'Warning: Tax Id already exist!';

$_['error_custom_field'] = '%s required!';